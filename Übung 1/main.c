#include <stdio.h>
#include <math.h>
#include "main.h"

int main()
{
    printf("Summe von 1 bis 20: %d\n", Aufgabe2_1(20));
    printf("Quadratsumme von 1 bis 20: %d\n", Aufgabe2_2(20));
    Aufgabe3(27);
    Aufgabe3(29);
    return 0;
}

int Aufgabe2_1(int num)
{
    // Nach Gaussscher Summenformel
    return (num) * (num + 1) / 2;
}

int Aufgabe2_2(int num)
{
    // Lösbar mit angepasster Summenformal, hier alternativ per Loop:
    int result = 0;
    for (int i = 1; i <= num; i++) {
	result += i * i;
    }
    return result;
}

void Aufgabe3(int num)
{
    printf("Ist %d eine Primzahl?\n", num);
    if (num < 0) {
	printf("Fehler: Nummer ist negativ.\n");
    } else if (num < 2) {	// Die kleinstmögliche Primzahl ist 2
	printf("Nein.\n");
    } else {
	for (int i = 2; i <= sqrt((double) num); i++) {	// Da alle möglichen Primfaktoren <n^0.5 geprüft werden kann die Schleife über n^0.5 terminiert werden.
	    if (num % i == 0) {
		printf("Nein.\n");
		return;
	    }
	}
	printf("Ja.\n");
    }
}
